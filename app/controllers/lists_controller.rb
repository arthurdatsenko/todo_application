class ListsController < ApplicationController
  before_action :authenticate_user!, except: [:index]

  def index
    @lists = current_user.lists if user_signed_in?
  end

  def new
    @list = current_user.lists.new
  end

  def create
    @list = current_user.lists.new list_params

    redirect_to lists_path, :notice => "List saved!" if @list.save
  end

  def edit
    @list = List.find params[:id]
  end

  def update
    @list = List.find params[:id]

    @list.update list_params

    redirect_to lists_path
  end

  def destroy
    @list = List.find params[:id]

    @list.destroy

    redirect_to lists_path
  end

  private

  def list_params
    params.require(:list).permit(:title)
  end

end
