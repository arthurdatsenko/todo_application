class Task < ApplicationRecord
  belongs_to :list

  def complete!
    self.completed = true
    save
  end

  validates :title, presence: {message: "can't be blank!"}

end
