class TasksController < ApplicationController
  helper_method :task, :list, :tasks

  def create
    task.assign_attributes(task_params)

    redirect_to list_tasks_path(list), :notice => "Task saved!" if task.save
  end

  def update
    task.update task_params

    redirect_to list_tasks_path(list)
  end

  def complete
    task.complete!

    redirect_to list_tasks_path(list)
  end

  def destroy
    task.destroy

    redirect_to list_tasks_path(list)
  end

  private

  delegate :tasks, to: :list, private: true

  def task
    @task ||= params[:id] ? tasks.find(params[:id]) : tasks.new
  end

  def list
    @list ||= List.find params[:list_id]
  end

  def task_params
    params.require(:task).permit(:title, :completed)
  end
end
